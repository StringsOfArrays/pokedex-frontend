## What this project does

This is a browser Pokedex application, build with React.
It shows a list of all currently available Pokemon with their base information, including Pokedex-number, name, an image and type(s).
Every Pokemon from this list is selectable and can bew viewed in a detail view. Here the user will see a representation of the area this pokemon originates from,
a description based on its first appearance, its pre-evolution, if there is one and information regarding its weight and height.

**Features:**

- Large, optimized list of all available Pokemon (around 900)
- Sort the list either alphabetical or by the Pokedex-number and filter it by a certain type
- The background of the selected Pokemon shows its origin region
- The App's background changes based on the time of day
- Responsible Design
- Loading Skeletons, to cater to modern user expectations
- Persist the latest selected Pokemon when the browser is refreshed

## Requirements and Setup

1. **yarn install** installs all the modules defined in the dependency section of the package.json
2. Start the server that this project requires to work and make sure the server runs **before** proceeding: https://gitlab.com/StringsOfArrays/pokedex-backend
3. **yarn start** will start this project on http://localhost:3000

## Technologies

React, Typescript, Apollo Client, GraphQL Codegen, React testing library, Material UI, Styled Components, React Virtualized
