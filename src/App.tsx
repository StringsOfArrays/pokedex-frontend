import React, { useState } from "react";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core";
import { ApolloProvider } from "@apollo/react-hooks";
import ApolloClient, { InMemoryCache } from "apollo-boost";
import PokemonList from "./components/pokemonList/PokemonList";
import DisplayedPokemon from "./components/displayedPokemon/DisplayedPokemon";
import BackgroundByDaytime from "./components/backgroundByDaytime/BackgroundByDaytime";
import { Pokemon } from "./graphql/pokemon/generated/apolloComponents";
import { Key, PokeTypes } from "./reusables";
import OptionButtonGroup from "./components/optionButtonGroup/OptionButtonGroup";

export const client = new ApolloClient({
  uri: "http://localhost:4000",
  cache: new InMemoryCache(),
});

const theme = createMuiTheme({
  typography: {
    fontFamily: `"Montserrat", sans-serif`,
    htmlFontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 700,
  },
});

function App() {
  const [selectedName, setSelectedName] = useState("");
  const [isListReady, setIsReady] = useState(false);
  const [key, setKey] = useState<Key | null>();
  const [filterType, setFilterType] = useState<PokeTypes | null>();
  const onClick = (name: string) => {
    if (name === selectedName) return;
    localStorage.setItem("selectedPokemon", name);
    setSelectedName(name!);
  };
  const comparator = (pokeLeft: Pokemon, pokeRight: Pokemon) => {
    if (!key) return 0;
    const left = key === "id" ? Number(pokeLeft[key]) : pokeLeft[key];
    const right = key === "id" ? Number(pokeRight[key]) : pokeRight[key];
    if (left > right) return 1;
    if (right > left) return -1;
    return 0;
  };
  const filter = (pokemon: Pokemon) => {
    if (!filterType) return true;
    const checkForType = pokemon.types?.find(
      (type) => type?.type?.name === filterType
    )?.type?.name;
    return checkForType === filterType;
  };

  return (
    <MuiThemeProvider theme={theme}>
      <ApolloProvider client={client}>
        <BackgroundByDaytime>
          {isListReady && (
            <OptionButtonGroup setKey={setKey} setFilter={setFilterType} />
          )}
          <PokemonList
            comparator={comparator}
            onClick={onClick}
            reportReady={setIsReady}
            filter={filter}
          />
          <DisplayedPokemon name={selectedName} cacheReady={isListReady} />
        </BackgroundByDaytime>
      </ApolloProvider>
    </MuiThemeProvider>
  );
}

export default App;
