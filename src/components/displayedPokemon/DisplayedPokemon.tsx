import React from "react";
import {
  Container,
  Grid,
  makeStyles,
  Paper,
  Typography,
} from "@material-ui/core";
import {
  Pokemon,
  useGetPokemonQuery,
} from "../../graphql/pokemon/generated/apolloComponents";
import {
  getGenAndRegionById,
  Frame,
  capitalizeString,
  ContainedImg,
  Spacer,
} from "../../reusables";
import styled from "styled-components";
import pokeballSprite from "../../images/Poké_Ball_icon.png";
import { client } from "../../App";
import { getAllPokemonQuery } from "../../graphql/queries/getPokemon";
import Skeleton from "@material-ui/lab/Skeleton";

const useStyles = makeStyles((theme) => ({
  verticalElement: {
    display: "flex",
    alignItems: "center",
    border: "1px solid black",
    justifyContent: "space-evenly",
  },
  container: {
    padding: "2rem 3rem",
    margin: "auto",
    [theme.breakpoints.up("lg")]: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
  },
  paper: {
    backgroundColor: "rgba(255, 255, 255, .8)",
  },
  portrait: {
    height: "100%",
    minHeight: "13rem",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: "rgba(255, 255, 255, 0.8)",
    borderRadius: "2%",
  },
  heading: {
    fontWeight: "bolder",
    [theme.breakpoints.down("md")]: {
      textAlign: "center",
      fontSize: 45,
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: 40,
    },
  },
}));

// This component displays the chosen Pokemon with additional information
const DisplayedPokemon: React.FunctionComponent<{
  name: string;
  cacheReady: boolean;
}> = ({ name, cacheReady }) => {
  const classes = useStyles();
  const selectedPokemon = name || localStorage.getItem("selectedPokemon");
  const { loading, error, data } = useGetPokemonQuery({
    variables: { name: selectedPokemon ?? "bulbasaur" },
  });

  if (error) return <div>{error.message}</div>;
  if (loading || !data || !cacheReady)
    return (
      <Frame
        isLeftAligned={false}
        data-testid="loading-test"
        mobileMargin="4rem"
      >
        <DisplayPokemonLoadSkeleton classes={classes} />
      </Frame>
    );

  let cachedData: any = null;
  try {
    cachedData = client.readQuery({ query: getAllPokemonQuery });
  } catch {
    console.log(
      "If this is not a test, there is no cache even though there should be"
    );
  }

  const regionAndGen = getGenAndRegionById(Number(data.getPokemon.pokemon.id));
  const background = chooseBackgroundByGen(regionAndGen?.region ?? "kanto");
  const preEvolution: Pokemon = cachedData?.getAllPokemon?.find(
    (pokemon: Pokemon) => pokemon.name === data.getPokemon.evolves_from_species
  );

  return (
    <Frame isLeftAligned={false} backgroundImg={background} mobileMargin="4rem">
      <Container className={classes.container}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <PaddingWrapper padding="1rem">
                <Typography
                  variant="h2"
                  component="h1"
                  className={classes.heading}
                >
                  {capitalizeString(data.getPokemon.pokemon.name)}
                </Typography>
              </PaddingWrapper>
            </Paper>
          </Grid>

          <Grid item xs={12}>
            <Grid container spacing={2}>
              <Grid item lg={6} md={12} xs={12}>
                <Paper className={classes.portrait}>
                  <ContainedImg
                    src={
                      data.getPokemon.pokemon.sprites?.front_default ??
                      pokeballSprite
                    }
                    alt={`The Pokemon ${data.getPokemon.pokemon.name}`}
                    maxHeight="80%"
                    maxWidth="80%"
                  />
                </Paper>
              </Grid>

              <Grid item lg={6} md={12} xs={12}>
                <Paper className={classes.paper}>
                  <PaddingWrapper padding=".4rem">
                    <Typography variant="h5" component="h2">
                      First seen:
                    </Typography>
                    <Typography component="p">
                      Gen {regionAndGen?.gen}, {regionAndGen?.region} region
                    </Typography>
                  </PaddingWrapper>
                </Paper>
                {preEvolution && (
                  <>
                    <Spacer height=".2rem" />
                    <Paper className={classes.paper}>
                      <PaddingWrapper padding=".4rem">
                        <Typography variant="h5" component="h3">
                          Evolves from:
                        </Typography>
                        <FlexRow>
                          <Typography>
                            #{preEvolution.id}{" "}
                            {capitalizeString(preEvolution.name)}
                          </Typography>
                          <ContainedImg
                            src={
                              preEvolution.sprites?.front_default ??
                              pokeballSprite
                            }
                            alt={`The pre-evolution of ${data.getPokemon.pokemon.name}`}
                            height="50%"
                          />
                        </FlexRow>
                      </PaddingWrapper>
                    </Paper>
                  </>
                )}
              </Grid>
            </Grid>
          </Grid>

          <Grid item md={12} lg={8} xs={12}>
            <Paper className={classes.paper} style={{ minHeight: "13rem" }}>
              <PaddingWrapper padding=".4rem">
                <Typography component="h4" variant="h4">
                  Description
                </Typography>
                <Spacer height="3rem" />
                <Typography>
                  {data.getPokemon.description?.replace("\f", " ")}
                </Typography>
              </PaddingWrapper>
            </Paper>
          </Grid>
          <Grid item md={12} lg={4} xs={12}>
            <Paper
              className={classes.paper}
              style={{
                minHeight: "13rem",
              }}
            >
              <PaddingWrapper padding=".4rem">
                <Typography>Height:</Typography>
                <Typography>{(data.getPokemon.height ?? 10) / 10} m</Typography>
                <Spacer height="1rem" />
                <Typography>Weight:</Typography>
                <Typography>
                  {(data.getPokemon.weight ?? 10) / 10} kg
                </Typography>
              </PaddingWrapper>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </Frame>
  );
};

const DisplayPokemonLoadSkeleton: React.FunctionComponent<{
  classes: Record<string, string>;
}> = ({ classes }) => {
  return (
    <Container className={classes.container}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Skeleton variant="rect" height="8rem" />
        </Grid>
        <Grid item xs={12}>
          <Grid container spacing={2}>
            <Grid item lg={6} md={12} xs={12}>
              <Skeleton variant="rect" height="12rem" />
            </Grid>
            <Grid item lg={6} md={12} xs={12}>
              <Skeleton variant="rect" height="5rem" />
              <Spacer height="1rem" />
              <Skeleton variant="rect" height="6rem" />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Grid container spacing={2}>
            <Grid item lg={8} md={12} xs={12}>
              <Skeleton variant="rect" height="12rem" />
            </Grid>
            <Grid item lg={4} md={12} xs={12}>
              <Skeleton variant="rect" height="12rem" />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
};

const chooseBackgroundByGen = (region: string) => {
  return `url(${require(`../../images/Regions/${region.toLowerCase()}.jpg`)})`;
};

const PaddingWrapper = styled.div<{ padding: string }>`
  padding: ${(props) => props.padding};
`;

const FlexRow = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  width: 100%;
`;

export default DisplayedPokemon;
