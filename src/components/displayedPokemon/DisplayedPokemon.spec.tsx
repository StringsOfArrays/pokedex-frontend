import React from "react";
import { act, render, waitForElementToBeRemoved } from "@testing-library/react";
import { MockedProvider, MockLink } from "@apollo/react-testing";
import { getPokemonQuery } from "../../graphql/queries/getPokemon";
import DisplayedPokemon from "./DisplayedPokemon";
import { InMemoryCache } from "apollo-boost";

const mockCache = new InMemoryCache().restore({
  ROOT_QUERY: {
    getAllPokemon: [
      {
        pokemon: {
          id: 1,
          name: "bulbasaur",
          sprites: {
            front_default: "../../images/Poké_Ball_icon.png",
          },
          types: {
            type: {
              name: "grass",
            },
          },
        },
      },
    ],
  },
});

const mockedGetPokemonWithNoPreEvo = [
  {
    request: {
      query: getPokemonQuery,
      variables: { name: "bulbasaur" },
    },
    result: {
      data: {
        getPokemon: {
          __typename: "",
          color: "green",
          evolves_from_species: null,
          height: 10,
          weight: 100,
          description: "its a bulbasaur",
          pokemon: {
            __typename: "",
            id: 1,
            name: "bulbasaur",
            sprites: {
              __typename: "",
              front_default: "../../images/Poké_Ball_icon.png",
            },
            types: [
              {
                __typename: "",
                type: {
                  __typename: "",
                  name: "grass",
                },
              },
            ],
          },
        },
      },
    },
  },
];

const mockedGetPokemonWithPreEvo = [
  {
    request: {
      query: getPokemonQuery,
      variables: { name: "ivysaur" },
    },
    result: {
      data: {
        getPokemon: {
          __typename: "",
          color: "green",
          evolves_from_species: "bulbasaur",
          height: 10,
          weight: 100,
          description: "its a ivysaur",
          pokemon: {
            __typename: "",
            id: 1,
            name: "ivysaur",
            sprites: {
              __typename: "",
              front_default: "../../images/Poké_Ball_icon.png",
            },
            types: [
              {
                __typename: "",
                type: {
                  __typename: "",
                  name: "grass",
                },
              },
            ],
          },
        },
      },
    },
  },
];

describe("<DisplayedPokemon />", () => {
  it("renders in a loading state", () => {
    const displayedPokemon = render(
      <MockedProvider mocks={mockedGetPokemonWithNoPreEvo} addTypename={false}>
        <DisplayedPokemon cacheReady={false} name="bulbasaur" />
      </MockedProvider>
    );
    const loadingFrame = displayedPokemon.findByTestId("loading-test");
    expect(loadingFrame).not.toBe(null);
  });

  it("renders a displayable Pokemon", async () => {
    const displayedPokemon = render(
      <MockedProvider
        mocks={mockedGetPokemonWithNoPreEvo}
        link={new MockLink(mockedGetPokemonWithNoPreEvo)}
        addTypename={false}
        cache={mockCache}
      >
        <DisplayedPokemon cacheReady={true} name="bulbasaur" />
      </MockedProvider>
    );
    await waitForElementToBeRemoved(() =>
      displayedPokemon.getByTestId("loading-test")
    );
    const description = await displayedPokemon.findByText("its a bulbasaur");
    expect(description).not.toBe(null);
  });

  it("renders a displayable Pokemon and its pre-evolution", async () => {
    const displayedPokemon = render(
      <MockedProvider
        mocks={mockedGetPokemonWithPreEvo}
        addTypename={false}
        cache={mockCache}
        link={new MockLink(mockedGetPokemonWithPreEvo)}
      >
        <DisplayedPokemon cacheReady={true} name="ivysaur" />
      </MockedProvider>
    );
    await waitForElementToBeRemoved(() =>
      displayedPokemon.getByTestId("loading-test")
    );
    const preEvolution = displayedPokemon.findByText("Evolves from:");
    expect(preEvolution).not.toBe(null);
  });
});
