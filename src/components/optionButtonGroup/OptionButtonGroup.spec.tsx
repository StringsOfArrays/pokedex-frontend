import React from "react";
import { render } from "@testing-library/react";
import user from "@testing-library/user-event";
import OptionButtonGroup from "./OptionButtonGroup";
import { Key, PokeTypes } from "../../reusables";

describe("<OptionButtonGroup/>", () => {
  const mockSetKey = jest.fn((_key: Key | null) => {});
  const mockSetFilter = jest.fn((_filterType: PokeTypes | null) => {});
  it("Renders a group of buttons", () => {
    const btnGroup = render(
      <OptionButtonGroup setKey={mockSetKey} setFilter={mockSetFilter} />
    );
    expect(btnGroup).not.toBeNull();
  });
  it("Has clickable buttons", async () => {
    const { findByTestId } = render(
      <OptionButtonGroup setKey={mockSetKey} setFilter={mockSetFilter} />
    );
    const buttons = [
      await findByTestId("menu-button-test"),
      await findByTestId("sort-by-id-test"),
      await findByTestId("sort-by-name-test"),
    ];
    buttons.forEach((button) => user.click(button));
    expect(mockSetKey).toHaveBeenCalled();
  });
});
