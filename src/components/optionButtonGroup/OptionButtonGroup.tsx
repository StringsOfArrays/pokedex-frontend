import {
  Button,
  FormControl,
  Grid,
  InputLabel,
  makeStyles,
  MenuItem,
  Select,
  Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import styled from "styled-components";
import { capitalizeString, Key, PokeTypes } from "../../reusables";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    backgroundColor: "white",
    "@media(max-width: 960px)": {
      width: "200px",
    },
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  sortBtn: {
    background: "white",
    "@media(max-width: 960px)": {
      width: "200px",
      height: "80px",
    },
  },
  menuBtn: {
    background: "white",
  },
  commandLabel: {
    "@media(max-width: 960px)": {
      fontSize: 20,
      fontWeight: "bold",
    },
  },
}));

const allTypes: PokeTypes[] = [
  "normal",
  "fire",
  "water",
  "grass",
  "electric",
  "ice",
  "poison",
  "fighting",
  "ground",
  "flying",
  "psychic",
  "bug",
  "rock",
  "ghost",
  "dark",
  "dragon",
  "steel",
  "fairy",
];
const OptionButtonGroup: React.FunctionComponent<{
  setKey: (key: Key | null) => void;
  setFilter: (filterType: PokeTypes | null) => void;
}> = ({ setKey, setFilter }) => {
  const classes = useStyles();
  const [type, setType] = useState("none");
  const [isVisible, setIsVisible] = useState(false);
  return (
    <>
      <MenuButtonWrapper isOpen={isVisible}>
        <Button
          data-testid="menu-button-test"
          variant="contained"
          className={classes.menuBtn}
          onClick={() => setIsVisible(!isVisible)}
        >
          {isVisible ? "Hide" : "Show"} Menu
        </Button>
      </MenuButtonWrapper>
      <Banner isVisible={isVisible}>
        <Grid container style={{ height: "100%" }} alignItems="center">
          <Grid item xs={undefined} md={2} />
          <Grid item md={4} lg={3} xs={12}>
            <Aligner>
              <Typography className={classes.commandLabel}>Sort by</Typography>
              <ResponsiveFlexSpacer width="1rem" responsiveHeight="1rem" />
              <Button
                data-testid="sort-by-id-test"
                variant="contained"
                onClick={() => setKey("id")}
                className={classes.sortBtn}
              >
                Dex No.
              </Button>
              <ResponsiveFlexSpacer width="1rem" responsiveHeight="1rem" />
              <Button
                data-testid="sort-by-name-test"
                variant="contained"
                onClick={() => setKey("name")}
                className={classes.sortBtn}
              >
                A - Z
              </Button>
            </Aligner>
          </Grid>
          <Grid item md={4} xs={12}>
            <Aligner>
              <Typography className={classes.commandLabel}>
                Filter by
              </Typography>
              <ResponsiveFlexSpacer width="1rem" responsiveHeight="1rem" />
              <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel>Type</InputLabel>
                <Select
                  value={type}
                  onChange={(e) => setType(e.target.value as string)}
                  label="Type"
                >
                  <MenuItem value={type} onClick={() => setFilter(null)}>
                    None
                  </MenuItem>
                  {allTypes.map((type) => (
                    <MenuItem
                      key={`${type}-type`}
                      onClick={() => setFilter(type)}
                      value={type}
                    >
                      {capitalizeString(type)}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Aligner>
          </Grid>
        </Grid>
      </Banner>
    </>
  );
};

const MenuButtonWrapper = styled.div<{ isOpen: boolean }>`
  position: absolute;
  right: 5%;
  top: 1rem;
  z-index: 3;
  @media (max-width: 960px) {
    right: 10%;
    position: ${(props) => (props.isOpen ? "fixed" : "absolute")};
  }
  @media (max-width: 600px) {
    right: 5%;
  }
`;

const ResponsiveFlexSpacer = styled.div<{
  width: string;
  responsiveHeight: string;
}>`
  height: 100%;
  width: ${(props) => props.width};
  @media (max-width: 960px) {
    width: 100%;
    height: ${(props) => props.responsiveHeight};
  }
`;

const Aligner = styled.div`
  display: flex;
  align-items: center;
  @media (max-width: 960px) {
    flex-direction: column;
  }
`;

const Banner = styled.div<{ isVisible: boolean }>`
  display: ${(props) => (props.isVisible ? "block" : "none")};
  position: absolute;
  height: 5rem;
  width: 100%;
  background-color: white;
  top: 0;
  z-index: 2;
  @media (max-width: 960px) {
    height: 100vh;
    position: fixed;
    background-color: rgba(255, 255, 255, 0.9);
  }
`;

export default OptionButtonGroup;
