import React from "react";
import { render } from "@testing-library/react";
import BackgroundByDaytime from "./BackgroundByDaytime";

describe("<BackgroundByDaytime />", () => {
  it("renders without error", () => {
    const { findByTestId } = render(<BackgroundByDaytime />);
    const wrapper = findByTestId("bg-test");
    expect(wrapper).not.toBe(null);
  });
});
