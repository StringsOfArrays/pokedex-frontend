import React from "react";
import styled from "styled-components";

// This component is responsible for picking a fitting background color for the app based on the daytime
const BackgroundByDaytime: React.FunctionComponent = ({ children }) => {
  const backgroundMap = {
    morning:
      "linear-gradient(180deg, rgba(220,220,255,1) 0%, rgba(0,212,255,1) 100%);",
    midday:
      "linear-gradient(180deg, rgba(44,44,255,1) 0%, rgba(0,212,255,1) 100%);",
    evening:
      "linear-gradient(180deg, rgba(255,136,44,1) 0%, rgba(0,69,255,1) 100%);",
    night: "linear-gradient(180deg, rgba(0,0,0,1) 0%, rgba(4,0,255,1) 100%);",
  };

  const backgroundPicker = () => {
    const currentTime = new Date().getHours();
    if (currentTime > 5 && currentTime < 12) return backgroundMap.morning;
    if (currentTime >= 12 && currentTime < 18) return backgroundMap.midday;
    if (currentTime >= 18 && currentTime < 22) return backgroundMap.evening;
    return backgroundMap.night;
  };

  return (
    <ResponsiveBackground data-testid="bg-test" bg={backgroundPicker()}>
      {children}
    </ResponsiveBackground>
  );
};

const ResponsiveBackground = styled.div<{ bg: string }>`
  height: 100%;
  min-height: 100vh;
  background: ${(props) => props.bg};
  @media (max-width: 960px) {
    padding-top: 6rem;
    padding-bottom: 4rem;
  }
`;

export default BackgroundByDaytime;
