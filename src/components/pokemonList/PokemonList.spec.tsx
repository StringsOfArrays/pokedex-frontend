import React from "react";
import { render, waitForElementToBeRemoved } from "@testing-library/react";
import { MockedProvider } from "@apollo/react-testing";
import { getAllPokemonQuery } from "../../graphql/queries/getPokemon";
import PokemonList from "./PokemonList";
import { Pokemon } from "../../graphql/pokemon/generated/apolloComponents";

const mockGetAllPokemonQuery = [
  {
    request: {
      query: getAllPokemonQuery,
    },
    result: {
      data: {
        getAllPokemon: [
          {
            id: 1,
            name: "testMon1",
            types: [
              {
                type: {
                  name: "poison",
                },
              },
            ],
            sprites: {
              front_default: null,
            },
          },
          {
            id: 2,
            name: "testMon2",
            types: [
              {
                type: {
                  name: "electric",
                },
              },
            ],
            sprites: {
              front_default: null,
            },
          },
          {
            id: 3,
            name: "testMon3",
            types: [
              {
                type: {
                  name: "fairy",
                },
              },
            ],
            sprites: {
              front_default: null,
            },
          },
        ],
      },
    },
  },
];

describe("<PokemonList />", () => {
  const mockOnClick = (_url: string) => {};
  const mockReportReady = (_isReady: boolean) => {};
  const mockComparator = (_a: Pokemon, _b: Pokemon) => 0;
  const mockFilter = (_pokemon: Pokemon) => true;
  it("renders a list of Pokemon in a loading state", () => {
    const pkmnList = render(
      <MockedProvider mocks={mockGetAllPokemonQuery} addTypename={false}>
        <PokemonList
          reportReady={mockReportReady}
          onClick={mockOnClick}
          comparator={mockComparator}
          filter={mockFilter}
        />
      </MockedProvider>
    );
    const loadContainer = pkmnList.getByTestId("loading-test");
    expect(loadContainer).not.toBeNull();
  });

  it("renders a list of Pokemon and doesn't get stuck loading", async () => {
    const pkmnList = render(
      <MockedProvider mocks={mockGetAllPokemonQuery} addTypename={false}>
        <PokemonList
          reportReady={mockReportReady}
          onClick={mockOnClick}
          comparator={mockComparator}
          filter={mockFilter}
        />
      </MockedProvider>
    );

    await waitForElementToBeRemoved(() => pkmnList.getByTestId("loading-test"));
    expect(pkmnList).not.toBe(null);
  });
});
