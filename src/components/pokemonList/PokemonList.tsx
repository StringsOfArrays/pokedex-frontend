import React, { useRef } from "react";
import { Paper, Grid, Container, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import {
  Pokemon,
  useGetAllPokemonQuery,
} from "../../graphql/pokemon/generated/apolloComponents";
import {
  capitalizeString,
  importTypeIcon,
  pickColorByType,
  Frame,
  ContainedImg,
  Spacer,
  convertRemToPx,
} from "../../reusables";
import styled from "styled-components";
import pokeballSprite from "../../images/Poké_Ball_icon.png";
import Skeleton from "@material-ui/lab/Skeleton";
import {
  List,
  ListRowRenderer,
  AutoSizer,
  CellMeasurerCache,
  CellMeasurer,
} from "react-virtualized";

const cache = new CellMeasurerCache({ fixedWidth: true });

const useStyles = makeStyles((theme) => ({
  typo: {
    textAlign: "center",
    margin: "auto",
  },
  pokePlate: {
    width: "90%",
    display: "flex",
    height: "4rem",
    transition: "0.3s",
    "&:hover": {
      transform: "scale(1.03)",
      cursor: "pointer",
    },
    padding: "1rem",
    justifyContent: "space-between",
    alignContent: "center",
  },
}));

// This Component renders a List of Pokemon as a listing of easy to navigate plates
const PokemonList: React.FunctionComponent<{
  onClick: (name: string) => void;
  reportReady: (isReady: boolean) => void;
  comparator: (a: Pokemon, b: Pokemon) => number;
  filter: (pokemon: Pokemon) => boolean;
}> = ({ onClick, reportReady, comparator, filter }) => {
  const rowHeight = useRef(convertRemToPx(7));
  const classes = useStyles();
  const { loading, error, data } = useGetAllPokemonQuery({
    onCompleted: (data) => {
      if (data) reportReady(true);
    },
  });
  if (error) return <div>{error.message}</div>;
  const limit = 893; // this hardcoded integer might seem odd, but its simply the limit of where the api delivers relevant pokemon by currently Gen 8
  const validPokemon = data?.getAllPokemon
    .slice(0, limit)
    .filter(filter)
    .sort(comparator);

  // In case there is no response we show the user a loading skeleton
  if (loading || !validPokemon)
    return (
      <Frame isLeftAligned={true} noFlex={true} noScroll={true}>
        <Container data-testid="loading-test" style={{ marginTop: "1rem" }}>
          <Grid container spacing={1}>
            <GeneratedSkeletons amount={50} passedClass={classes.pokePlate} />
          </Grid>
        </Container>
      </Frame>
    );

  // The element the virtualized list will render
  const renderRow: ListRowRenderer = ({ index, parent, key, style }) => (
    <CellMeasurer
      key={key}
      cache={cache}
      parent={parent}
      rowIndex={index}
      columnIndex={0}
    >
      <div
        key={key}
        style={{
          ...style,
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Paper
          onClick={() => onClick(validPokemon[index].name)}
          className={classes.pokePlate}
          style={{
            background: pickColorByType(validPokemon[index].types),
          }}
        >
          <CircularElement diameter="4em">
            <Typography style={{ fontWeight: "bold" }} className={classes.typo}>
              #{validPokemon[index].id}
            </Typography>
          </CircularElement>
          <Typography className={classes.typo} style={{ fontWeight: "bold" }}>
            {capitalizeString(validPokemon[index].name ?? "")}
          </Typography>
          <ContainedImg
            src={validPokemon[index].sprites?.front_default ?? pokeballSprite}
            alt="a sprite of the pokemon"
            maxWidth="125px"
          />
          <IconContainer>
            {validPokemon[index].types?.map((type, index) => (
              <React.Fragment key={index}>
                <ContainedImg
                  src={importTypeIcon(type?.type?.name ?? "")}
                  alt={`The icon for the ${type?.type?.name} type`}
                  maxHeight="20px"
                />
                {validPokemon[index].types?.length &&
                  validPokemon[index].types!.length > 1 &&
                  index < validPokemon[index].types!.length - 1 && (
                    <Spacer height="10px" />
                  )}
              </React.Fragment>
            ))}
          </IconContainer>
        </Paper>
      </div>
    </CellMeasurer>
  );

  return (
    <Frame isLeftAligned={true} noFlex={true} noScroll={true}>
      <AutoSizer>
        {({ height, width }) => {
          const desiredRowHeight = convertRemToPx(7);
          if (rowHeight.current !== desiredRowHeight) {
            rowHeight.current = desiredRowHeight;
            cache.clearAll();
          }
          return (
            <List
              rowHeight={rowHeight.current}
              rowCount={validPokemon?.length ?? 0}
              height={height}
              width={width}
              rowRenderer={renderRow}
              overscanRowCount={3}
              deferredMeasurementCache={cache}
              style={{ outline: "none", padding: "2rem" }}
            />
          );
        }}
      </AutoSizer>
    </Frame>
  );
};

const GeneratedSkeletons: React.FunctionComponent<{
  amount: number;
  passedClass: string;
}> = ({ amount, passedClass }) => {
  const array = Array(amount)
    .fill(0)
    .map((_elem, index) => (
      <LoadSkeleton key={index} passedClass={passedClass} />
    ));
  return <>{array}</>;
};

const LoadSkeleton: React.FunctionComponent<{
  passedClass: string;
}> = ({ passedClass }) => {
  return (
    <Grid item xs={12}>
      <Paper className={passedClass}>
        <Skeleton variant="circle" width="4em" height="4em" />
        <Skeleton variant="text" width="200px" height="50px" />
        <Skeleton variant="rect" width="125px" height="100%" />
      </Paper>
    </Grid>
  );
};

const CircularElement = styled.div<{ diameter: string; color?: string }>`
  background: ${(props) => props.color ?? "white"};
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  height: ${(props) => props.diameter};
  width: ${(props) => props.diameter};
  @media (max-width: 1200px) and (min-width: 960px) {
    display: none;
  }
  @media (max-width: 600px) {
    display: none;
  }
`;

const IconContainer = styled.div`
  max-height: 100%;
  max-width: 200px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  @media (max-width: 320px) {
    display: none;
  }
`;

export default PokemonList;
