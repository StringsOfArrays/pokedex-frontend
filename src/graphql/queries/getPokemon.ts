import { gql } from "apollo-boost";

export const getPokemonQuery = gql`
  query GetPokemon($name: String, $id: Int) {
    getPokemon(input: { name: $name, id: $id }) {
      color
      evolves_from_species
      height
      weight
      description
      pokemon {
        id
        name
        sprites {
          front_default
        }
        types {
          type {
            name
          }
        }
      }
    }
  }
`;

export const getAllPokemonQuery = gql`
  query GetAllPokemon {
    getAllPokemon {
      id
      name
      types {
        type {
          name
        }
      }
      sprites {
        front_default
      }
    }
  }
`;
