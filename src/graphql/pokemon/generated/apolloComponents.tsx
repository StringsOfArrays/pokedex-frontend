import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: any }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Query = {
  __typename?: 'Query';
  hello: Scalars['String'];
  getPokemon: ExtendedPokemon;
  getAllPokemon: Array<Pokemon>;
};


export type QueryHelloArgs = {
  name?: Maybe<Scalars['String']>;
};


export type QueryGetPokemonArgs = {
  input?: Maybe<GetPokemonInput>;
};

export type GetPokemonInput = {
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
};

export type ExtendedPokemon = {
  __typename?: 'ExtendedPokemon';
  pokemon: Pokemon;
  color?: Maybe<Scalars['String']>;
  height?: Maybe<Scalars['Int']>;
  weight?: Maybe<Scalars['Int']>;
  evolves_from_species?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
};

export type Pokemon = {
  __typename?: 'Pokemon';
  id: Scalars['String'];
  name: Scalars['String'];
  types?: Maybe<Array<Maybe<SlotAndListType>>>;
  sprites?: Maybe<Sprites>;
};

export type SlotAndListType = {
  __typename?: 'SlotAndListType';
  slot?: Maybe<Scalars['Int']>;
  type?: Maybe<ListType>;
};

export type ListType = {
  __typename?: 'ListType';
  name: Scalars['String'];
  url: Scalars['String'];
};

export type Sprites = {
  __typename?: 'Sprites';
  back_default?: Maybe<Scalars['String']>;
  back_female?: Maybe<Scalars['String']>;
  back_shiny?: Maybe<Scalars['String']>;
  back_shiny_female?: Maybe<Scalars['String']>;
  front_default?: Maybe<Scalars['String']>;
  front_female?: Maybe<Scalars['String']>;
  front_shiny?: Maybe<Scalars['String']>;
  front_shiny_female?: Maybe<Scalars['String']>;
};

export type ListPokemon = {
  __typename?: 'ListPokemon';
  name: Scalars['String'];
  url: Scalars['String'];
};

export type GetPokemonQueryVariables = Exact<{
  name?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['Int']>;
}>;


export type GetPokemonQuery = (
  { __typename?: 'Query' }
  & { getPokemon: (
    { __typename?: 'ExtendedPokemon' }
    & Pick<ExtendedPokemon, 'color' | 'evolves_from_species' | 'height' | 'weight' | 'description'>
    & { pokemon: (
      { __typename?: 'Pokemon' }
      & Pick<Pokemon, 'id' | 'name'>
      & { sprites?: Maybe<(
        { __typename?: 'Sprites' }
        & Pick<Sprites, 'front_default'>
      )>, types?: Maybe<Array<Maybe<(
        { __typename?: 'SlotAndListType' }
        & { type?: Maybe<(
          { __typename?: 'ListType' }
          & Pick<ListType, 'name'>
        )> }
      )>>> }
    ) }
  ) }
);

export type GetAllPokemonQueryVariables = Exact<{ [key: string]: never; }>;


export type GetAllPokemonQuery = (
  { __typename?: 'Query' }
  & { getAllPokemon: Array<(
    { __typename?: 'Pokemon' }
    & Pick<Pokemon, 'id' | 'name'>
    & { types?: Maybe<Array<Maybe<(
      { __typename?: 'SlotAndListType' }
      & { type?: Maybe<(
        { __typename?: 'ListType' }
        & Pick<ListType, 'name'>
      )> }
    )>>>, sprites?: Maybe<(
      { __typename?: 'Sprites' }
      & Pick<Sprites, 'front_default'>
    )> }
  )> }
);


export const GetPokemonDocument = gql`
    query GetPokemon($name: String, $id: Int) {
  getPokemon(input: {name: $name, id: $id}) {
    color
    evolves_from_species
    height
    weight
    description
    pokemon {
      id
      name
      sprites {
        front_default
      }
      types {
        type {
          name
        }
      }
    }
  }
}
    `;

/**
 * __useGetPokemonQuery__
 *
 * To run a query within a React component, call `useGetPokemonQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPokemonQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPokemonQuery({
 *   variables: {
 *      name: // value for 'name'
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetPokemonQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetPokemonQuery, GetPokemonQueryVariables>) {
        return ApolloReactHooks.useQuery<GetPokemonQuery, GetPokemonQueryVariables>(GetPokemonDocument, baseOptions);
      }
export function useGetPokemonLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetPokemonQuery, GetPokemonQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetPokemonQuery, GetPokemonQueryVariables>(GetPokemonDocument, baseOptions);
        }
export type GetPokemonQueryHookResult = ReturnType<typeof useGetPokemonQuery>;
export type GetPokemonLazyQueryHookResult = ReturnType<typeof useGetPokemonLazyQuery>;
export type GetPokemonQueryResult = ApolloReactCommon.QueryResult<GetPokemonQuery, GetPokemonQueryVariables>;
export const GetAllPokemonDocument = gql`
    query GetAllPokemon {
  getAllPokemon {
    id
    name
    types {
      type {
        name
      }
    }
    sprites {
      front_default
    }
  }
}
    `;

/**
 * __useGetAllPokemonQuery__
 *
 * To run a query within a React component, call `useGetAllPokemonQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllPokemonQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllPokemonQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetAllPokemonQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetAllPokemonQuery, GetAllPokemonQueryVariables>) {
        return ApolloReactHooks.useQuery<GetAllPokemonQuery, GetAllPokemonQueryVariables>(GetAllPokemonDocument, baseOptions);
      }
export function useGetAllPokemonLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetAllPokemonQuery, GetAllPokemonQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetAllPokemonQuery, GetAllPokemonQueryVariables>(GetAllPokemonDocument, baseOptions);
        }
export type GetAllPokemonQueryHookResult = ReturnType<typeof useGetAllPokemonQuery>;
export type GetAllPokemonLazyQueryHookResult = ReturnType<typeof useGetAllPokemonLazyQuery>;
export type GetAllPokemonQueryResult = ApolloReactCommon.QueryResult<GetAllPokemonQuery, GetAllPokemonQueryVariables>;