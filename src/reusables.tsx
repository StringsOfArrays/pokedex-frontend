import styled from "styled-components";
import { ListType, Maybe } from "./graphql/pokemon/generated/apolloComponents";

// A collection of functions, types and styled components that are needed in multiple places

export const Frame = styled.div<{
  isLeftAligned: boolean;
  backgroundImg?: string;
  mobileMargin?: string;
  noFlex?: boolean;
  noScroll?: boolean;
}>`
  width: 40%;
  height: 80%;
  position: absolute !important;
  left: ${(props) => (props.isLeftAligned ? 5 : 55)}%;
  bottom: 5%;
  border-radius: 2%;
  overflow-x: hidden;
  overflow-y: ${(props) => (props.noScroll ? "hidden" : "auto")};
  display: ${(props) => (props.noFlex ? "block" : "flex")};
  justify-content: center;
  background: ${(props) => props.backgroundImg ?? "#f5f5f5"} center no-repeat;
  background-size: cover;
  padding: 1rem;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  scroll-behavior: smooth;
  @media (max-width: 960px) {
    position: static !important;
    display: block;
    width: 80%;
    height: 100vh;
    margin: auto;
    margin-top: ${(props) => props.mobileMargin ?? 0};
  }
  @media (max-width: 600px) {
    position: static !important;
    width: 90%;
    height: 50vh;
    margin: auto;
    margin-top: ${(props) => props.mobileMargin ?? 0};
  }
`;

export const ContainedImg = styled.img<{
  maxHeight?: string;
  maxWidth?: string;
}>`
  max-width: ${(props) => props.maxWidth};
  max-height: ${(props) => props.maxHeight};
  object-fit: contain;
`;

export const Spacer = styled.div<{ height: string }>`
  height: ${(props) => props.height};
`;

export const colorMap: Record<string, string> = {
  normal: "#b6b6a8",
  poison: "#9a5692",
  psychic: "#f261ae",
  grass: "#8cd751",
  ground: "#eccb56",
  ice: "#96f1ff",
  fire: "#fa5643",
  rock: "#cdbc72",
  dragon: "#8774ff",
  water: "#57b0ff",
  bug: "#c3d21f",
  dark: "#856451",
  fighting: "#a85642",
  ghost: "#7773d3",
  steel: "#c4c2db",
  flying: "#79a4ff",
  electric: "#fde53c",
  fairy: "#fde53c",
};

export const pickColorByType = (
  types:
    | Maybe<
        {
          __typename?: "SlotAndListType" | undefined;
        } & {
          type?:
            | ({
                __typename?: "ListType" | undefined;
              } & Pick<ListType, "name">)
            | null
            | undefined;
        }
      >[]
    | null
    | undefined,
  deg = 127
) => {
  if (!types) return "";
  const colors = types.map(
    (type) => type?.type && colorMap[type?.type?.name] + " 70%"
  );
  return colors.length > 1
    ? `linear-gradient(${deg}deg, ${colors})`
    : colors[0] ?? "";
};

export const capitalizeString = (input: string) => {
  return input.charAt(0).toUpperCase() + input.slice(1);
};

export const importTypeIcon = (dir: string) => {
  const image = require(`./images/typeIcons/${dir}.png`);
  if (image) return image;
  console.error(`no image found at: ${dir}`);
  return "";
};

export const convertRemToPx = (rem: number) => {
  const fontSize = window.getComputedStyle(
    document.getElementsByTagName("html")[0]
  ).fontSize;
  return rem * Number(fontSize.replace("px", ""));
};

export const getGenAndRegionById = (id: number) => {
  if (id <= 151) return { gen: 1, region: "Kanto" };
  if (id <= 251) return { gen: 2, region: "Johto" };
  if (id <= 386) return { gen: 3, region: "Hoenn" };
  if (id <= 493) return { gen: 4, region: "Sinnoh" };
  if (id <= 649) return { gen: 5, region: "Unova" };
  if (id <= 721) return { gen: 6, region: "Kalos" };
  if (id <= 809) return { gen: 7, region: "Alola" };
  if (id <= 893) return { gen: 8, region: "Galar" };
};

export type Key = "id" | "name";
export type PokeTypes =
  | "normal"
  | "fire"
  | "water"
  | "grass"
  | "electric"
  | "ice"
  | "poison"
  | "fighting"
  | "ground"
  | "flying"
  | "psychic"
  | "bug"
  | "rock"
  | "ghost"
  | "dark"
  | "dragon"
  | "steel"
  | "fairy";
